#/bin/bash
fallocate -l 1G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
cp /etc/fstab /etc/fstab.bak
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

apt-get --assume-yes remove snapd
apt-get --assume-yes update
apt-get --assume-yes upgradepython
apt-get --assume-yes install mc ncdu htop nginx redis nodejs g++ gcc make zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 autoconf libgdbm-dev libncurses5-dev automake libtool bison pkg-config libffi-dev imagemagick memcached libpq-dev yarn php-cli php-gd php-pgsql php-fpm php-imap php-zip php-curl php-xml php-mysql php-mbstring php-memcache php-memcached php-sqlite3 php-readline composer

sed -i -e 's@fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;@fastcgi_param  SCRIPT_FILENAME    $realpath_root$fastcgi_script_name;@g' /etc/nginx/fastcgi.conf

apt-get --assume-yes install mysql-server
apt-get --assume-yes install postgresql
sed -i -e 's@bind-address\t\t= 127.0.0.1@bind-address\t\t= 0.0.0.0@g' /etc/mysql/mysql.conf.d/mysqld.cnf
sed -i -e "s@\#listen_addresses = 'localhost'@listen_addresses = '*'\t@g" /etc/postgresql/12/main/postgresql.conf

