<?php

$user_pass = randomPassword();
$mysql_pass = randomPassword();
$pg_pass = randomPassword();
$home_dir = "/home/{$argv[1]}";
$php_domain = $argv[2] ? $argv[2] : $argv[1] . '-php.localhost';
$php_home_dir = $home_dir . '/php_site/public';
$ruby_domain = $argv[3] ? $argv[3] : $argv[1] . '-ruby.localhost';
$ruby_home_dir = $home_dir . '/ruby_site/public';

$php_search = [
	'{{HOME_DIR}}',
	'{{PHP_HOME_DIR}}',
	'{{RUBY_HOME_DIR}}',
	'{{PHP_SITE_HOSTNAMES}}',
	'{{RUBY_SITE_HOSTNAMES}}',
	'{{LINUX_USERNAME}}',
	'{{PHP_INDEX_HTML_LOCATION}}',
	'{{RUBY_INDEX_HTML_LOCATION}}',
];

$php_replace = [
	$home_dir,
	$php_home_dir,
	$ruby_home_dir,
	$php_domain,
	$ruby_domain,
	"{$argv[1]}",
	$php_home_dir,
	$ruby_home_dir,
];

function randomPassword( $length = 8 )
{
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $length = rand(10, 16);
        $password = substr( str_shuffle(sha1(rand() . time()) . $chars ), 0, $length );
        return $password;
}

shell_exec("useradd {$argv[1]}");
shell_exec("mkdir /home/{$argv[1]}");
shell_exec("chmod 750 {$home_dir}");
shell_exec("chown -R {$argv[1]}:{$argv[1]} {$home_dir}");
shell_exec("usermod -d {$home_dir} -s /bin/bash {$argv[1]}");
shell_exec("usermod -a -G {$argv[1]} www-data");
shell_exec("echo \"{$argv[1]}:{$user_pass}\" | chpasswd");
shell_exec("echo \"{$argv[1]}    ALL=NOPASSWD: /usr/sbin/service unicorn_{$argv[1]} *\" >> /etc/sudoers.d/{$argv[1]}_limited");

$con = mysqli_connect("localhost", "root");
mysqli_query($con, "CREATE DATABASE {$argv[1]}_db");
mysqli_query($con, "CREATE USER '{$argv[1]}'@'%' IDENTIFIED BY '{$mysql_pass}'");
mysqli_query($con, "ALTER USER '{$argv[1]}'@'%' IDENTIFIED WITH mysql_native_password BY '{$mysql_pass}';");
mysqli_query($con, "GRANT ALL PRIVILEGES ON {$argv[1]}_db.* TO '{$argv[1]}'@'%'");
mysqli_query($con, "FLUSH PRIVILEGES;");

shell_exec('su postgres bash -c "psql -c \"create database ' . $argv[1] . '_db;\""');
shell_exec('su postgres bash -c "psql -c \"create user ' . $argv[1] . ' with encrypted password \'' . $pg_pass . '\';\""');
shell_exec('su postgres bash -c "psql -c \"grant all privileges on database ' . $argv[1] . '_db to ' . $argv[1] . ';\""');

shell_exec('echo "host    ' . $argv[1] . '_db        ' . $argv[1] . '           0.0.0.0/0               md5" >> /etc/postgresql/12/main/pg_hba.conf');
shell_exec('service postgresql restart');


$nginx_template_content = file_get_contents('templates/nginx_site_template');

$nginx_template_content = str_replace($php_search, $php_replace, $nginx_template_content);

$nginx_php_host_file = "/etc/nginx/sites-available/{$argv[1]}";
$handle = fopen($nginx_php_host_file, 'w') or die('Cannot open file:  ' . $nginx_php_host_file);
fwrite($handle, $nginx_template_content);
fclose($handle);


$fpm_template_content = file_get_contents('templates/fpm_template');

$fpm_template_content = str_replace($php_search, $php_replace, $fpm_template_content);

$fpm_config_file = "/etc/php/7.4/fpm/pool.d/{$argv[1]}.conf";
$handle = fopen($fpm_config_file, 'w') or die('Cannot open file:  ' . $fpm_config_file);
fwrite($handle, $fpm_template_content);
fclose($handle);

$unicorn_template_content = file_get_contents('templates/unicorn_template');

$unicorn_template_content = str_replace($php_search, $php_replace, $unicorn_template_content);

$unicorn_config_file = "/etc/init.d/unicorn_{$argv[1]}";
$handle = fopen($unicorn_config_file, 'w') or die('Cannot open file:  ' . $unicorn_config_file);
fwrite($handle, $unicorn_template_content);
fclose($handle);


shell_exec("mkdir -p {$php_home_dir}");

$index_html_template_content = file_get_contents("templates/index_html_template");
$index_html_template_content = str_replace($php_search, $php_replace, $index_html_template_content);
$php_index_html_file = "{$php_home_dir}/index.html";
$handle = fopen($php_index_html_file, 'w') or die('Cannot open file:  ' . $php_index_html_file);
fwrite($handle, $index_html_template_content);
fclose($handle);



$index_php_template_content = file_get_contents("templates/index_php_template");
$php_index_php_file = "{$php_home_dir}/index.php";
$handle = fopen($php_index_php_file, 'w') or die('Cannot open file:  ' . $php_index_php_file);
fwrite($handle, $index_php_template_content);
fclose($handle);

shell_exec("mkdir /home/{$argv[1]}/logs");
shell_exec("chown -R {$argv[1]}:{$argv[1]} {$home_dir}");
shell_exec("ln -s {$nginx_php_host_file} /etc/nginx/sites-enabled/");
shell_exec("service nginx reload");
shell_exec("service php7.4-fpm reload");

shell_exec("chmod 0700 {$unicorn_config_file}");
shell_exec("chown {$argv[1]}:{$argv[1]} {$unicorn_config_file}");
shell_exec("update-rc.d unicorn_{$argv[1]} defaults");

$result_string = "\n" . "Linux User: {$argv[1]} Pass {$user_pass}\n" . "Postgres User: {$argv[1]} Pass {$pg_pass} Database {$argv[1]}_db\n"  . "Mysql User: {$argv[1]} Pass {$mysql_pass} Database {$argv[1]}_db\n to install ruby\n\n\curl -L https://get.rvm.io | bash -s -- --ignore-dotfiles --autolibs=0 --ruby
\necho \"source /home/{$argv[1]}/.rvm/scripts/rvm\" >> ~/.bash_profile\nsource /home/{$argv[1]}/.rvm/scripts/rvm\n";
$credentials_file = "{$home_dir}/.htcred";
$handle = fopen($credentials_file, 'w') or die('Cannot open file:  ' . $credentials_file);
fwrite($handle, $result_string);
fclose($handle);

echo $result_string;


