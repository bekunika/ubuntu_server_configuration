<?php
$home_dir = "/home/{$argv[1]}";
$nginx_filename = "{$argv[1]}";
$fpm_config_filename = "/etc/php/7.4/fpm/pool.d/{$argv[1]}.conf";

shell_exec("userdel -f {$argv[1]}");
shell_exec("groupdel -f {$argv[1]}");
shell_exec("rm -R {$home_dir}");
$con = mysqli_connect("localhost", "root");
mysqli_query($con, "DROP DATABASE {$argv[1]}_db");
mysqli_query($con, "DROP USER '{$argv[1]}'@'%'");

shell_exec("rm /etc/nginx/sites-enabled/{$nginx_filename}");
shell_exec("rm /etc/nginx/sites-available/{$nginx_filename}");
shell_exec("rm {$fpm_config_filename}");
shell_exec("rm /etc/init.d/unicorn_{$argv[1]}");
shell_exec("rm /etc/sudoers.d/{$argv[1]}_limited");
shell_exec("service nginx reload");
shell_exec("service php7.4-fpm reload");
shell_exec("service unicorn_{$argv[1]} stop");
shell_exec("update-rc.d unicorn_{$argv[1]} remove");
shell_exec("systemctl daemon-reload");


